from django.shortcuts import render, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect

from .models import artist, artists_localinfo
from artwork.models import artwork

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

def artists(request, pk):
	art_artist = artist.objects.get(pk = pk)
	if art_artist:
		info = artists_localinfo.objects.get(a_id = art_artist.pk)
		if info:
			artworks = artwork.objects.filter(a_id = art_artist)
			paginator = Paginator(artworks, 6)
			page = request.GET.get('page')
			try:
				art_list = paginator.page(page)
			except PageNotAnInteger:
				art_list = paginator.page(1)
			except EmptyPage:
				art_list = paginator.page(paginator.num_pages)
			return render(request, 'countries.html', {
				'name': info.name,
				'description': info.description,
				'artworks': art_list
			})
	return render(request, 'countries.html', {
		'name': 'No data',
		'description': "There is no data available for this artist, sorry.",
	})
