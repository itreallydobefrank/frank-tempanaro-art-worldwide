from django.contrib import admin
from .models import artwork, artworks_localinfo,medium,mediums_localinfo
# Register your models here.

admin.site.register(medium)
admin.site.register(mediums_localinfo)
admin.site.register(artwork)
admin.site.register(artworks_localinfo)
