from django.shortcuts import render, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from artwork.models import artwork
from login.models import list, list_content

from .forms import ListForm

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

@login_required
def lists(request, pk):
	user_lists = list.objects.filter(up_id = request.user.user_profile.pk)
	
	l = user_lists.get(pk = int(pk))
	if l:
		artworks = l.contents.all()
		paginator = Paginator(artworks, 6)
		page = request.GET.get('page')
		try:
			art_list = paginator.page(page)
		except PageNotAnInteger:
			art_list = paginator.page(1)
		except EmptyPage:
			art_list = paginator.page(paginator.num_pages)
		return render(request, 'lists.html', {
			'list': l,
			'name': l.name,
			'description': 'Contents of the list "' + l.name + '".',
			'artworks': art_list
		})
	return HttpResponseRedirect('/index/')

@login_required
def edit_list(request, pk):
	user_lists = list.objects.filter(up_id = request.user.user_profile.pk)
	
	l = user_lists.get(pk = int(pk))
	if l:
		if request.method == 'POST':
			list_form = ListForm(request.POST, instance = l)
			if list_form.is_valid():
				list_form.save()
				
				return HttpResponseRedirect('/lists/' + str(pk) + '/')
		form = ListForm(instance = l)
		
		return render(request, 'edit_list.html', {'form': form, 'list': l})
	else:
		return HttpResponseRedirect('/lists/')

@login_required
def likes(request):
	artworks = request.user.user_profile.likes.all()
	paginator = Paginator(artworks, 6)
	page = request.GET.get('page')
	try:
		art_list = paginator.page(page)
	except PageNotAnInteger:
		art_list = paginator.page(1)
	except EmptyPage:
		art_list = paginator.page(paginator.num_pages)
	return render(request, 'likes.html', {
		'artworks': art_list
	})

@login_required
def list_overview(request):
	if request.method == 'POST':
		list_form = ListForm(request.POST)
		if list_form.is_valid():
			new_list = list_form.save(commit=False)
			new_list.up_id = request.user.user_profile
			new_list.type = 0
			new_list.save()
			
			return HttpResponseRedirect('/lists/')
	form = ListForm()
	
	return render(request, 'list_overview.html', {'form': form})
