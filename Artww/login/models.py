from django.db import models
from datetime import timedelta
from django.utils.timezone import now
from parameterized import parameterized
from index.models import language
from django.contrib.auth.models import User
from django.conf import settings
from artwork.models import artwork

class user_profile(models.Model):
    image_path = models.ImageField(upload_to='static/images/', blank=True)
    display_name = models.CharField(max_length=100)
    profile_visible = models.BooleanField()
    preferred_lang = models.ForeignKey(language, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    students = models.ManyToManyField('self', blank=True)
    likes = models.ManyToManyField(artwork, blank=True)

class list(models.Model):
    name = models.CharField(max_length=100)
    up_id=models.ForeignKey(user_profile, on_delete=models.CASCADE)
    contents = models.ManyToManyField(artwork, blank=True)

class list_content(models.Model):
	pass
