from django.shortcuts import render, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.

def search(request):
    return render(request, 'search.html')
