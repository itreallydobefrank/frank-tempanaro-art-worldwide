from django.contrib import admin
from .models import artist, artists_localinfo
# Register your models here.

admin.site.register(artist)
admin.site.register(artists_localinfo)
