from django.shortcuts import render, redirect, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect
from .forms import RegistrationProfileForm
from django.contrib.auth.forms import UserCreationForm

def profile(request):
	profile = request.user.user_profile
	if request.method == 'POST':
		savedForm = RegistrationProfileForm(request.POST, instance = profile)
		if savedForm.is_valid():
			savedForm.save()
	form = RegistrationProfileForm(instance = profile)
	
	return render(request, 'profile.html', {'form': form})

def register(request):
	if request.method == 'POST':
		user_form = UserCreationForm(request.POST, prefix='user')
		profile_form = RegistrationProfileForm(request.POST, prefix='profile')
		if user_form.is_valid() and profile_form.is_valid():
			user = user_form.save(commit=False)
			profile = profile_form.save(commit=False)
			
			profile.user = user
			
			user.save()
			profile.save()
			
			return HttpResponseRedirect('/accounts/login/')
	user_form = UserCreationForm(prefix='user')
	profile_form = RegistrationProfileForm(prefix='profile')
	
	return render(request, 'register.html', {'user_form': user_form, 'profile_form': profile_form})
