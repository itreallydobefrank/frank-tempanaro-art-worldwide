from django.db import models
from datetime import timedelta
from django.utils.timezone import now
from parameterized import parameterized
from index.models import language
# Create your models here.
class continent(models.Model):
	pass

class country(models.Model):
	code = models.CharField(max_length=2)
	cn_id=models.ForeignKey(continent, on_delete=models.CASCADE)

class continents_localinfo(models.Model):
	name = models.CharField(max_length=100)
	cn_id=models.ForeignKey(continent, on_delete=models.CASCADE)
	l_id=models.ForeignKey(language, on_delete=models.CASCADE)

class countries_localinfo(models.Model):
	name = models.CharField(max_length=100)
	description = models.CharField(max_length=1000)
	c_id=models.ForeignKey(country, on_delete=models.CASCADE)
	l_id=models.ForeignKey(language, on_delete=models.CASCADE)
