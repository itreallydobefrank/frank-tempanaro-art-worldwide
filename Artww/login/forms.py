from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import user_profile

class RegistrationUserForm(ModelForm):
	class Meta:
		model = User
		fields = ["username", "password", "email"]

class RegistrationProfileForm(ModelForm):
	class Meta:
		model = user_profile
		fields = ["display_name", "profile_visible", "preferred_lang"]
