# Art Worldwide

## Install Python requirements

```bash
pip install -r requirements.txt
```
## Apply migrations
```bash
python Artww/manage.py migrate
```
## Run the Django development server
```bash
python Artww/manage.py runserver
```


## SECRET_KEY
A secret key should be generated and placed into a local settings file, instead of being in settings.py. The Gitlab CI script will create its own local_settings.py file with a random secret key, but if you want to run things locally you'll need to generate your own local_settings.py with the following shell command:
```bash
echo "SECRET_KEY='$(python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')'" > Artww/Artww/local_settings.py
```
That one-liner generates a secret key and places it in the local settings file.

## Run tests locally
```bash
python Artww/manage.py test
```
To add additional tests, modify each app's tests.py file with new classes that inherit from TestCase. Django's built-in testing framework is used, so you can reference [Django's own tutorial on testing](https://docs.djangoproject.com/en/3.1/topics/testing/overview/).
