from django.db import models
from datetime import timedelta
from django.utils.timezone import now
from parameterized import parameterized
from countries.models import country
from index.models import language
# Create your models here.
class artist(models.Model):
    image_path = models.ImageField(upload_to='static/images/')
    url = models.CharField(max_length=500)
    birthDATE= models.DateTimeField('date birthday',null=True, blank=True)
    birth_accuracy=models.IntegerField()
    c_id=models.ForeignKey(country, on_delete=models.CASCADE)

class artists_localinfo(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    a_id=models.ForeignKey(artist, on_delete=models.CASCADE)
    l_id=models.ForeignKey(language, on_delete=models.CASCADE)
