from django.shortcuts import render, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect

from artists.models import artist
from artwork.models import artwork
from .models import country, countries_localinfo

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

def countries(request, code):
	c = country.objects.filter(code = code).first()
	if c:
		info = countries_localinfo.objects.get(c_id = c.pk)
		if info:
			artists = artist.objects.filter(c_id = c.pk).values_list('pk', flat=True)
			artworks = artwork.objects.filter(a_id__in=artists)
			paginator = Paginator(artworks, 6)
			page = request.GET.get('page')
			try:
				art_list = paginator.page(page)
			except PageNotAnInteger:
				art_list = paginator.page(1)
			except EmptyPage:
				art_list = paginator.page(paginator.num_pages)
			return render(request, 'countries.html', {
				'name': info.name,
				'description': info.description,
				'artworks': art_list
			})
	return render(request, 'countries.html', {
		'name': 'No data',
		'description': "There is no data available for this country, sorry! Go back to the home page and try another country.",
	})
