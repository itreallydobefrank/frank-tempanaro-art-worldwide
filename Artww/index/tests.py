from django.test import TestCase
from django.test import Client
from .models import language
from parameterized import parameterized


class LanguageModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        langu = language.objects.create(name='Here is a test Name')

    def test_name_field_value(self):
        langu = language.objects.create(name='Here is a test Name')
        self.assertEqual('Here is a test Name', langu.name)

    @parameterized.expand([
        ("name1", "name1"),
        ("name2", "name2"),
        ("name3", "name3"),
    ])
    def test_name_field_value_parameterized(self, input, expected):
        langu = language.objects.create(name=input)
        self.assertEqual(langu.name, expected)

    def test_indexView(self):
        response = self.client.get('/index/')
        self.assertEqual(response.status_code, 200)
