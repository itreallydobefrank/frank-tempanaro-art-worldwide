from django.test import*
from parameterized import*
from django.test import TestCase
from django.test import Client
from index.models import language
from .models import continent, country
from parameterized import parameterized
# Create your tests here.
class ArtistModelTest(TestCase):
    @classmethod
    def test_artistView(self):
        response = self.client.get('/artists/')
        self.assertEqual(response.status_code, 200)
