from django.shortcuts import render, HttpResponse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from login.models import list, list_content

from .models import artwork as a, artworks_localinfo, medium, mediums_localinfo
from artists.models import artist, artists_localinfo

def artwork_main(request, pk):
	art = a.objects.get(pk = int(pk))
	info = artworks_localinfo.objects.get(aw_id = art.pk)
	
	liked = False
	try:
		if request.user.user_profile:
			if request.user.user_profile.likes.filter(id = art.pk):
				liked = True
	except AttributeError:
		pass
	
	if info:
		art_artist = art.a_id
		artist_info = artists_localinfo.objects.get(a_id = art_artist.pk)
		
		return render(request, 'artwork.html', {
			'artinfo': info,
			'artistinfo': artist_info,
			'artwork': art,
			'liked': liked
		})

@login_required
def artwork_list_add(request, pk):
	art = a.objects.get(pk = int(pk))
	info = artworks_localinfo.objects.get(aw_id = art.pk)

	return render(request, 'artwork_list_add.html', {
		'name': info.name,
		'artwork': art
	})

@login_required
def artwork_do_add(request, pk, list_pk):
	user_lists = list.objects.filter(up_id = request.user.user_profile.pk)
	
	l = user_lists.get(pk = int(list_pk))
	if l:
		l.contents.add(a.objects.get(pk = int(pk)))
	return HttpResponseRedirect('/artwork/' + str(pk) + '/')

@login_required
def artwork_like(request, pk):
	art = a.objects.get(pk = int(pk))
	request.user.user_profile.likes.add(art)
	return HttpResponseRedirect('/artwork/' + str(pk) + '/')

@login_required
def artwork_dislike(request, pk):
	art = a.objects.get(pk = int(pk))
	request.user.user_profile.likes.remove(art)
	return HttpResponseRedirect('/artwork/' + str(pk) + '/')
