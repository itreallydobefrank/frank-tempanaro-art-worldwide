from django.urls import include, path
from . import views

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('profile/', views.profile),
    path('register/', views.register),
]
