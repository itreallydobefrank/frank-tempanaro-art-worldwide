from django.contrib import admin
from .models import continent,country,continents_localinfo,countries_localinfo
# Register your models here.

admin.site.register(continent)
admin.site.register(country)
admin.site.register(continents_localinfo)
admin.site.register(countries_localinfo)
