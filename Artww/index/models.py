from django.db import models
from datetime import timedelta
from django.utils.timezone import now
from parameterized import parameterized
# Create your models here.
class language(models.Model):
	name = models.CharField(max_length=100)
	def __str__(self):
		return self.name
