from django.urls import path
from . import views

urlpatterns = [
	path('<int:pk>/', views.artwork_main, name='Artwork'),
	path('<int:pk>/addlist/', views.artwork_list_add, name='ArtworkListAdd'),
	path('<int:pk>/addto/<int:list_pk>/', views.artwork_do_add, name='ArtworkAdd'),
	path('<int:pk>/like/', views.artwork_like, name='ArtworkLike'),
	path('<int:pk>/dislike/', views.artwork_dislike, name='ArtworkDislike'),
]
