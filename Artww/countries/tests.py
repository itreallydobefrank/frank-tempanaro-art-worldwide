from django.test import TestCase
from django.test import Client
from index.models import language
from .models import continent, country
from parameterized import parameterized


class ContinentModelTest(TestCase):

    @classmethod
    def setTestContinent(cls):
        continent1 = continent.objects.create(id=1)
        country1 = country.objects.create(cn_id=continent1, id=2)

    def test_country_id_value(self):
        continent1 = continent.objects.create(id=1)
        country1 = country.objects.create(cn_id=continent1, id=2)
        self.assertEqual(2, country1.id)

    def test_continent_id_value(self):
        continent1 = continent.objects.create(id=1)
        self.assertEqual(1, continent1.id)

    @parameterized.expand([
        ( 13, 13),
        ( 2, 2),
        ( 24, 24),
    ])
    def test_continent_id_value_parameterized(self, continent_input, expected):
        continent1 = continent.objects.create(id=continent_input)
        self.assertEqual(expected, continent1.id)

    @parameterized.expand([
        (12, 13, 12),
        (1, 2, 1),
        (23, 24, 23),
    ])
    def test_country_id_value_parameterized(self, country_input, continent_input,  expected):
        continent1 = continent.objects.create(id=continent_input)
        country1 = country.objects.create(cn_id=continent1, id=country_input)
        self.assertEqual(expected, country1.id)

    def test_countriesView(self):
        response = self.client.get('/countries/ES/')
        self.assertEqual(response.status_code, 200)
