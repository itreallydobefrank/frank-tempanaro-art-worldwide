from django.urls import path
from . import views


urlpatterns = [
    # path('', 'apps.Search.views.Search', name='Search'),
    path('', views.search, name='Search'),
]
