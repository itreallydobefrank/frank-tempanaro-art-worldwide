from django.urls import path
from . import views

urlpatterns = [
	path('', views.list_overview, name='ListAll'),
	path('<int:pk>/', views.lists, name='Lists'),
	path('likes/', views.likes, name='Likes'),
	path('<int:pk>/edit/', views.edit_list, name='EditList'),
]
