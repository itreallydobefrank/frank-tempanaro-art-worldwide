from django.forms import ModelForm
from login.models import list

class ListForm(ModelForm):
	class Meta:
		model = list
		fields = ["name"]
