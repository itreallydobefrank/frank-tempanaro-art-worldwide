from django.db import models
from datetime import timedelta
from django.utils.timezone import now
from parameterized import parameterized
from artists.models import artist
from index.models import language
# Create your models here.
class medium(models.Model):
    pass
class mediums_localinfo(models.Model):
    name = models.CharField(max_length=100)
    m_id=models.ForeignKey(medium, on_delete=models.CASCADE)
    l_id=models.ForeignKey(language, on_delete=models.CASCADE)

class artwork(models.Model):
    image_path = models.ImageField(upload_to='static/images/')
    url = models.CharField(max_length=500)
    createdDATE= models.DateTimeField('date created',null=True, blank=True)
    created_accuracy=models.IntegerField()
    a_id=models.ForeignKey(artist, on_delete=models.CASCADE)
    m_id=models.ForeignKey(medium, on_delete=models.CASCADE)

class artworks_localinfo(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    aw_id=models.ForeignKey(artwork, on_delete=models.CASCADE)
    l_id=models.ForeignKey(language, on_delete=models.CASCADE)
